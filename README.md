# What Tags

`what_tags` is a little tool for use in CI environments that calculates Docker tags to apply to an image based on the git information of the commit being built.

- It will always produce a tag that is the SHA of the current commit, truncated to 8 characters.
- If we are on a branch, it also produces a tag that is the name of the branch.
- If we are on a git-tagged commit, it also produces a matching docker tag.
- If that tag is a semantic version, it also checks to see if it is the latest semantic version of a major or major.minor release, and if so it produces tags for those components.
- If that semver is the most recent version, it also produces the docker tag "latest".

All tags produced will be slugified for use in docker - only lower-case alphanumerics, dashes, and periods, and at most 128 characters.

## Usage

```sh
what_tags | xargs -I{} docker tag [your built image here] [registry.example.com/image/path]:{}
what_tags | xargs -I{} docker push [registry.example.com/image/path]:{}
```
