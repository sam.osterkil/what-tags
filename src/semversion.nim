import options
import strformat
import strutils
from regex import re, match, RegexMatch, groupFirstCapture

type Version* = object
  major*: int
  minor*: int
  patch*: int
  prerelease*: Option[string]
  metadata*: Option[string]

let semverRegex = re"^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$"

proc parseVersion*(semverCandidate: string): Option[Version] =
  var m: RegexMatch
  if semverCandidate.match(semverRegex, m):
    let prerelease = m.groupFirstCapture("prerelease", semverCandidate)
    let metadata = m.groupFirstCapture("buildmetadata", semverCandidate)
    result = some(Version(
      major: parseInt(m.groupFirstCapture("major", semverCandidate)),
      minor: parseInt(m.groupFirstCapture("minor", semverCandidate)),
      patch: parseInt(m.groupFirstCapture("patch", semverCandidate)),
      prerelease: if prerelease != "": some(prerelease) else: none(string),
      metadata: if metadata != "": some(metadata ) else: none(string)

    ))

func `$`(x: Version): string =
  result = fmt"{x.major}.{x.minor}.{x.patch}"
  if x.prerelease.isSome:
    result = result & fmt"-{x.prerelease}"
  if x.metadata.isSome:
    result = result & fmt"+{x.prerelease}"


func cmpVersion*(a: Version, b: Version): int =
  if a.major - b.major != 0:
    result = a.major - b.major
  elif a.minor - b.minor != 0:
    result = a.minor - b.minor
  else:
    result = a.patch - b.patch
