import algorithm
import cligen
import gitapi
import options
import sequtils
import strformat
import sugar

from regex import replace, re
from strutils import toLowerAscii, startsWith, isEmptyOrWhitespace

import semversion

func slugify(input: string): string =
  result = input.toLowerAscii().replace(re"[^a-z0-9.-]", "-")
  if result.len > 128:
    result = result[0..127]

func emptyToOption*(x: string): Option[string] =
  if x.len != 0:
    result = some(x)

proc getDockerTags*(sha: string, headTags: seq[string], allTags: seq[string], branch: Option[string]): seq[string] =
  result = @[sha[0..7]]
  for headTag in headTags:
    assert headTag in allTags
    result.add(headTag)
    let versionCandidate = parseVersion(headTag)
    if versionCandidate.isSome:
      let headSemver = versionCandidate.get
      # This tag is a semver!  Let's see which subcomponents we need.
      # First, collect all the git tags that are semversions.
      let allSemvers: seq[Version] = collect(newSeq):
        for d in allTags:
          let p = parseVersion(d)
          if p.isSome: p.get

      # If headSemver is the greatest semver with its Major version, it should apply that Major version as a docker tag.
      if allSemvers.filter((x: Version) => x.major == headSemver.major).sorted(cmpVersion)[^1] == headSemver:
        result.add(fmt"{headSemver.major}")

      # Similarly with minor.
      if allSemvers.filter((x: Version) => x.major == headSemver.major and x.minor == headSemver.minor).sorted(cmpVersion)[^1] == headSemver:
        result.add(fmt"{headSemver.major}.{headSemver.minor}")

      # And if it's the biggest of ALL semvers, apply "latest"
      if allSemvers.sorted(cmpVersion)[^1] == headSemver:
        result.add("latest")

  if branch.isSome:
    result.add(branch.get)
  result = result.map(slugify)

when isMainModule:
  proc what_tags(gitRepoLocation: string="."): int=
    let repo = createGitRepo(gitRepoLocation)
    if "fatal" in repo.gitCommand("status"):
      stderr.writeLine(repo.gitCommand("status"))
      stderr.writeLine("(what-tags operates on git tags, are you running it inside a checked-out git repo?)")
      return 1

    let sha = repo.gitId()
    let branch = repo.gitCommand("rev-parse", "--abbrev-ref", "HEAD").strip().emptyToOption()
    let tags = repo.gitTags()
    let headTag = repo.gitCommand("tag", "--points-at=HEAD").split('\n').map((s) => s.strip())
    for dockerTag in getDockerTags(sha, headTag, tags, branch).filter((d) => not d.isEmptyOrWhitespace()):
      echo dockerTag

  dispatch(what_tags)
