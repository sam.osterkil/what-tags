import what_tags

import options
import strutils
import unittest

suite "getDockerTags":

  test "when given boring values for sha, tag, and branch, they are all in the output":
    let allTags: seq[string] = getDockerTags(
      "da54030c296e52ad7bc2a55af547150241f932e2",
      @["boring-tag"],
      @["boring-tag"],
      some("boring-branch")
    )

    check("boring-tag" in allTags)
    check("boring-branch" in allTags)
    check("da54030c" in allTags) # truncated to 8 chars

  test "when the branch name contains special and uppercased characters, it is slugified in the output":
    let allTags: seq[string] = getDockerTags(
      "da54030c296e52ad7bc2a55af547150241f932e2",
      @[],
      @[],
      some("This?Branch/Needs'To$Be-slugified")
    )

    check("this-branch-needs-to-be-slugified" in allTags)

  test "when the branch name is more than 128 characters, it is truncated":
    let allTags: seq[string] = getDockerTags(
      "da54030c296e52ad7bc2a55af547150241f932e2",
      @["1.2.3"],
      @["1.2.3"],
      some("a".repeat(500))
    )
    check("a".repeat(500) notin allTags)
    check("a".repeat(128) in allTags)


  test "when there is only one git tag and it is a semver, all of its subcomponents are in the produced tags":
    let allTags: seq[string] = getDockerTags(
      "da54030c296e52ad7bc2a55af547150241f932e2",
      @["1.2.3"],
      @["1.2.3"],
      some("boring-branch")
    )
    check("1.2.3" in allTags)
    check("1.2" in allTags)
    check("1" in allTags)

  test "when there are multiple 1.x.y tags and HEAD is the latest of them, \"1\" is in the output":
    let allTags: seq[string] = getDockerTags(
      "da54030c296e52ad7bc2a55af547150241f932e2",
      @["1.2.3"],
      @["1.0.0", "1.1.1", "1.2.3",],
      some("boring-branch")
    )
    check("1.2.3" in allTags)
    check("1.2" in allTags)
    check("1" in allTags)

  test "when there are multiple 1.x.y tags and HEAD is NOT the latest of them, \"1\" is NOT in the output":
    let allTags: seq[string] = getDockerTags(
      "da54030c296e52ad7bc2a55af547150241f932e2",
      @["1.2.3"],
      @["1.0.0", "1.1.1", "1.2.3", "1.4.8"],
      some("boring-branch")
    )
    check("1.2.3" in allTags)
    check("1.2" in allTags)
    check("1" notin allTags)

  test "when there are multiple 1.2.y tags and HEAD is the latest of them, \"1.2\" is in the output":
    let allTags: seq[string] = getDockerTags(
      "da54030c296e52ad7bc2a55af547150241f932e2",
      @["1.2.3"],
      @["1.0.0", "1.1.1", "1.2.0", "1.2.1", "1.2.3", "1.3.3"],
      some("boring-branch")
    )

    check("1.2.3" in allTags)
    check("1.2" in allTags)
    check("1" notin allTags)

  test "when there are multiple 1.2.y tags and HEAD is NOT the latest of them, \"1.2\" is NOT in the output":
    let allTags: seq[string] = getDockerTags(
      "da54030c296e52ad7bc2a55af547150241f932e2",
      @["1.2.3"],
      @["1.0.0", "1.1.1", "1.2.3", "1.2.4", "1.4.8"],
      some("boring-branch")
    )
    check("1.2.3" in allTags)
    check("1.2" notin allTags)
    check("1" notin allTags)

  test "when the HEAD is the latest git tag, it also produces the tag \"latest\".":
    let allTags: seq[string] = getDockerTags(
      "da54030c296e52ad7bc2a55af547150241f932e2",
      @["1.2.3"],
      @["1.0.0", "1.1.1", "1.2.3"],
      some("boring-branch")
    )
    check("latest" in allTags)

  test "when the HEAD is NOT the latest git tag, it does NOT produce the tag \"latest\".":
    let allTags: seq[string] = getDockerTags(
      "da54030c296e52ad7bc2a55af547150241f932e2",
      @["1.2.3"],
      @["1.0.0", "1.1.1", "1.2.3", "1.2.4"],
      some("boring-branch")
    )
    check("latest" notin allTags)

  test "when there are multiple tags applied to HEAD, they are all produced":
    let allTags: seq[string] = getDockerTags(
      "da54030c296e52ad7bc2a55af547150241f932e2",
      @["1.2.3", "potato"],
      @["1.0.0", "1.1.1", "1.2.3", "1.2.4", "potato"],
      some("boring-branch")
    )
    check("1.2.3" in allTags)
    check("potato" in allTags)

  test "when there are multiple tags applied to HEAD and ONE of them is the latest, \"latest\", \"1\", and \"1.2\" are all produced":
    let allTags: seq[string] = getDockerTags(
      "da54030c296e52ad7bc2a55af547150241f932e2",
      @["1.2.3", "potato"],
      @["1.0.0", "1.1.1", "1.2.3", "potato"],
      some("boring-branch")
    )
    check("latest" in allTags)
    check("1.2.3" in allTags)
    check("1.2" in allTags)
    check("1" in allTags)
    check("potato" in allTags)
