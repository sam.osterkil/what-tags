# Package

version       = "0.1.0"
author        = "Sam Osterkil"
description   = "A tool for producing docker tags from git tags"
license       = "MIT"
srcDir        = "src"
bin           = @["what_tags"]


# Dependencies

requires "nim >= 1.4.0", "gitapi", "regex >= 0.18.0", "cligen >= 1.3.2"
